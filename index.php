 <?php
 //including header file
 include_once "header.php";
 include_once "AutoLoad.php";

 ?>

    <body>
      <div  class="container-fluid">
      <h3>Product list</h3>
        <!-- select for action select -->
        <div  class="input-group action-control">
          <select class="form-control">
            <option selected value="1">Mass delete action</option>
          </select>
          <span>
            <!-- action apply button -->
            <button id="action_button" class="btn btn-default" type="button">Apply</button>
          </span>
        </div>
      <!-- separator horizontal line -->
      <hr class="horizontal-line">
      <!-- Product List -->
      <?php
      //instantiating database object
      $database = new Database();
      // gets database connection
      $db = $database->getConnection();
      // injects database connection in to product object
      $products = new Products($db);
      // displays all products from db
      $products->readAll();
       ?>

       <div id="test"></div>
      </div>

      <script>
      // on action button press find all checked checkboxes
      $("#action_button").click(function(event){
        event.preventDefault();
        var IDs = [];
        // finds all checked checkboxes and saves there value to an array IDs
        $(".product input:checkbox:checked").map(function(){
            IDs.push($(this).val());
        });
        //console.log(IDs);
        // ajax call for mass deletion action
        $.ajax({
            url: 'AJAXaction.php',
            method: "POST",
            data: {
                'delete': 1,
                'arrayOfIds': IDs
            },
            success: function (data){
              //on success reload page
              location.reload();
            }

        });
      });
      </script>


    </body>
    </html>
