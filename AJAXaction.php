<?php
include_once "AutoLoad.php";
// dynamicly changes forms special property based on class
if(isset($_POST['changeClass']))
{
  try
  {
    $class = ProductFactory::getClass($_POST['classType']);
    $class->formHtml();
  }
  catch(Exception $e){
    echo $e->getMessage();
  }

}

if(isset($_POST['delete'])){
  $array = $_POST['arrayOfIds'];
  //echo var_dump($array);
  //instantiate database object
  $database = new Database();
  // gets database connection
  $db = $database->getConnection();
  // injects database connection in to product object
  $products = new Products($db);
  // delete selected products
  $products->delete($array);
}
