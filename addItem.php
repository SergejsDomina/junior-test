<?php
// including header file and AutoLoad
include_once "header.php";
include_once "AutoLoad.php";
// if
if(isset($_POST["data"])){
  $array = $_POST["data"];
  //var_dump($array);
  $form_not_filed = false;
  //checking array for empty filds
  foreach ($array as $key => $value) {
          if (empty($value)) {
              //seting bool $form_not_filed to true if one fild is empty
              $form_not_filed = true;
              //breaks foreach loop
              break;
          }
      }
  // if array has no empty filds try to create product
  if($form_not_filed == false){
    try
    {
      // get products class(by type)
      $product = ProductFactory::getClass($_POST['data']['type']);
      // sets up the object variables
      $product->setUp($array);
      // creates database object
      $database = new Database();
      // gets database connection
      $db = $database->getConnection();
      // injects database connection in to product object
      $product->setDb($db);
      // trys to create product, if successfull tells user
      if($product->create()){
           echo "<div class=\"alert alert-success alert-dismissable\">";
               echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
               echo "Product was created.";
           echo "</div>";
       }

       // if unable to create the product, tell the user
       else{
           echo "<div class=\"alert alert-danger alert-dismissable\">";
               echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
               echo "Unable to create Product.";
           echo "</div>";
           }

    }catch(Exception $e){
      //displays Exception messege
      echo $e->getMessage();
    }
    // if some filds in form var not filled, tells the user
  }else{
    echo "<div class=\"alert alert-danger alert-dismissable\">";
        echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
        echo "Some filds seem to be empty.";
    echo "</div>";
  }

}

?>
<body>
  <div  class="container-fluid">
  <h3>Product list</h3>
    <!-- select for action select -->
    <div  class="action-control">
        <!-- action apply button -->
        <button id="Button" class="btn btn-default" type="button">Save</button>
    </div>
  <!-- separator horizontal line -->
  <hr class="horizontal-line">
  <!-- Product creationg form -->
  <form id="Form" method="POST">
    <!-- Displays sku input field -->
    <div class="form-group row">
      <label for="inputSku" class="col-sm-1 col-form-label">SKU</label>
      <div class="col-sm-5">
        <input type="text" name="data[sku]" class="form-control" id="inputSku" placeholder="Stock keeping unit...">
      </div>
      <div class="col-sm-6"></div>
    </div>
    <!--  name input field -->
    <div class="form-group row">
      <label for="inputName" class="col-sm-1 col-form-label">Name</label>
      <div class="col-sm-5">
          <input type="text" name="data[name]"  class="form-control" id="inputName" placeholder="Product name...">
        <div class="col-sm-6"></div>
      </div>
    </div>
    <!--  Price input field -->
    <div class="form-group row">
      <label for="inputPrice" class="col-sm-1 col-form-label">Price</label>
      <div class="col-sm-5">
          <input type="number" step=0.01 name="data[price]"  class="form-control" id="inputPrice" placeholder="Product price...(100.00)">
        <div class="col-sm-6"></div>
      </div>
    </div>
    <!--  type switcher field -->
    <div class="form-group row">
      <label for="typeSwitch" class="col-sm-1 col-form-label">Type Switcher</label>
      <div class="col-sm-5">
        <select id="typeSwitch" name="data[type]" class="form-control">
          <option  hidden selected value> -- select type -- </option>
          <option  value="Disc">Disc</option>
          <option  value="Book">Book</option>
          <option  value="Furniture">Furniture</option>
        </select>
        <div class="col-sm-6"></div>
      </div>
    </div>
    <!-- special property -->
    <div id="spec_prop"></div>

  </form>

  </div>

<script>
//function witch changes object type dinamicly
$("#typeSwitch").change(function() {
  //gets selected type
  var val = $("#typeSwitch").val();
  // changes filds dynamicly with ajax call
  $.ajax({
      url: 'AJAXaction.php',
      method: "POST",
      data: {
          'changeClass': 1,
          'classType': val
      },
      success: function (data){
        //on success appens html
          $('#spec_prop').html(data);
      }

  });
});
// submits form on buttons "Save" press
$(document).ready(function() {
       $("#Button").click(function() {
           $("#Form").submit();
       });
    });

</script>


</body>
</html>
