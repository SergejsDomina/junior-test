<?php
class Furniture implements ProductInterface
{
  // declaring veriables
  private $db;
  private $id;
  private $sku;
  private $name;
  private $price;
  private $type;
  private $height;
  private $width;
  private $length;

  // seting up Database connection
  public function setDb($dataBase)
  {
    $this->db = $dataBase;
  }
  // create new Product in database
  public function create()
  {
    $query = "INSERT INTO
      product
      SET
      sku = ?, name = ?, price = ?, type = ?,
      dim_height = ?, dim_width = ?, dim_length =?";

        $stmt = $this->db->prepare($query);

        $stmt->bindParam(1, $this->sku);
        $stmt->bindParam(2, $this->name);
        $stmt->bindParam(3, $this->price);
        $stmt->bindParam(4, $this->type);
        $stmt->bindParam(5, $this->height);
        $stmt->bindParam(6, $this->width);
        $stmt->bindParam(7, $this->length);

        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
  }

  // seting oject up
  public function setUp($array)
  {
    // checking, does array include ID (if array is coming from db)
    if(array_key_exists('id', $array)){
      $this->id = $array['id'];
    }
    $this->sku = $array['sku'];
    $this->name = $array['name'];
    $this->price = $array['price'];
    $this->type = $array['type'];
    $this->height = $array['dim_height'];
    $this->width = $array['dim_width'];
    $this->length = $array['dim_length'];
  }

  // render object
  public function displayProduct(){
    echo "<div class='col-md-2 product'>
      <input type='checkbox' class='check-box' name='delete' value='" . $this->id . "'/>
      <p>" . $this->sku . "</p>
      <p>" . $this->name . "</p>
      <p>" . $this->price . " $</p>
      <p>Dimensions: " . $this->height . "x" . $this->width . "x" . $this->length . " </p>
      </div>";
  }

  public function formHtml()
  {
    echo " <div class='form-group row'>
        <label for='inputHeight' class='col-sm-1 col-form-label'>Height</label>
        <div class='col-sm-5'>
            <input type='number' name='data[dim_height]'  class='form-control' id='inputHeight' placeholder='Furniture height'>
          <div class='col-sm-6'></div>
        </div>
      </div>
      <div class='form-group row'>
          <label for='inputWidth' class='col-sm-1 col-form-label'>Width</label>
          <div class='col-sm-5'>
              <input type='number' name='data[dim_width]'  class='form-control' id='inputWidth' placeholder='Furniture width'>
            <div class='col-sm-6'></div>
          </div>
        </div>
        <div class='form-group row'>
            <label for='inputLength' class='col-sm-1 col-form-label'>Length</label>
            <div class='col-sm-5'>
                <input type='number' name='data[dim_length]'  class='form-control' id='inputLength' placeholder='Furniture length'>
              <div class='col-sm-6'></div>
            </div>
          </div>";
  }
}
