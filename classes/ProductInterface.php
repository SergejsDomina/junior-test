<?php

interface ProductInterface
{
  // displays special property form filds in additem.php
  public function formHtml();
  // sets up object parameters(veriables) from array()
  public function setUp($array);
  // renders produkt to page
  public function displayProduct();
  // creates new entery in db from given object
  public function create();
  // sets the corrent database connection
  public function setDb($dataBase);
}
