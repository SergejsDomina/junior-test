<?php
class Disc implements ProductInterface
{
  // declaring veriables
  private $db;
  private $id;
  private $sku;
  private $name;
  private $price;
  private $type;
  private $size;

  // seting up Database connection
  public function setDb($dataBase)
  {
    $this->db = $dataBase;
  }
  // create new Product in database
  public function create()
  {
    // product creation query
    $query = "INSERT INTO
      product
      SET
      sku = ?, name = ?, price = ?, type = ?,
      d_size = ?";
        // prepares query
        $stmt = $this->db->prepare($query);
        // binds parameters to query
        $stmt->bindParam(1, $this->sku);
        $stmt->bindParam(2, $this->name);
        $stmt->bindParam(3, $this->price);
        $stmt->bindParam(4, $this->type);
        $stmt->bindParam(5, $this->size);

        //trys to create product
        if($stmt->execute()){
          // on success return true
            return true;
        }else{
          // if unsuccessfull return false
            return false;
        }
  }

  // seting oject up
  public function setUp($array)
  {
    // checking, does array include ID (if array is coming from db)
    if(array_key_exists('id', $array)){
      $this->id = $array['id'];
    }
    $this->sku = $array['sku'];
    $this->name = $array['name'];
    $this->price = $array['price'];
    $this->type = $array['type'];
    $this->size = $array['d_size'];
  }
  //render object
  public function displayProduct(){
    echo "<div class='col-md-2 product'>
      <input type='checkbox' class='check-box' name='delete' value='" . $this->id . "'/>
      <p>" . $this->sku . "</p>
      <p>" . $this->name . "</p>
      <p>" . $this->price . " $</p>
      <p>Size: " . $this->size . "MB</p>
      </div>";
  }
  //html for form in additem.php
  public function formHtml()
  {
    echo " <div class='form-group row'>
        <label for='inputSize' class='col-sm-1 col-form-label'>Disc Size</label>
        <div class='col-sm-5'>
            <input type='number' name='data[d_size]' class='form-control' id='inputSize' placeholder='Disc size in Mb...'>
          <div class='col-sm-6'></div>
        </div>
      </div> ";
  }
}
