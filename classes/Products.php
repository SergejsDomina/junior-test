<?php
include_once "AutoLoad.php";
class Products
{
  private $db;


  public function __construct($database)
  {
    // sets up database connection
    $this->db = $database;
  }
  // displays all products
  public function readAll()
  {

    // prepare the query
    $stmt = $this->db->prepare( "SELECT * FROM product" );
    $stmt->execute();
    $result = $stmt->fetchAll( PDO::FETCH_ASSOC );
    // if result is not empty then render products on page
    if (!empty($result))
    {
        //for each row in rezults create a obj of product by type
        foreach( $result as $row )
        {
          // instantiate a new instance of product by type
          $product = ProductFactory::getClass($row['type']);
          // sets up the object variables
          $product->setUp($row);
          // renders product on page
          $product->displayProduct();

        }

    }

  }
  //function to delete one or multiple products at the same time (takes in an array of id`s)
  public function delete($array)
  {
    //assign id`s to array values
    $values = $array;
    //assign id veriable
    $id = '';
    // prepare the query
    $stmt = $this->db->prepare("DELETE FROM product WHERE id=:id");
    //bind the veriable $id with bindParam
    $stmt->bindParam(":id", $id);
    // iterate thru array changing $id value
    foreach($values as $id) {
      $stmt->execute();
    }
  }
}
