<?php

class ProductFactory
{
  // static function witch returns requested class from a string(class name)
  public static function getClass($type)
  {
    //if class exists return new instance if this class
    if(class_exists($type))
    {
      return new $type();
    }
    // if it does not, trow a Exception
    throw new Exception("There was a problem loading selected type");
  }
}
