<?php
class Book implements ProductInterface
{
  // declaring veriables
  private $db;
  private $id;
  private $sku;
  private $name;
  private $price;
  private $type;
  private $weight;

  //seting up Database connection
  public function setDb($dataBase)
  {
    $this->db = $dataBase;
  }
  //create new Product in database
  public function create()
  {
    $query = "INSERT INTO
      product
      SET
      sku = ?, name = ?, price = ?, type = ?,
      b_weight = ?";

        $stmt = $this->db->prepare($query);

        $stmt->bindParam(1, $this->sku);
        $stmt->bindParam(2, $this->name);
        $stmt->bindParam(3, $this->price);
        $stmt->bindParam(4, $this->type);
        $stmt->bindParam(5, $this->weight);

        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
  }

  //seting oject up
  public function setUp($array)
  {
    // checking, does array include ID (if array is coming from db)
    if(array_key_exists('id', $array)){
      $this->id = $array['id'];
    }
    $this->sku = $array['sku'];
    $this->name = $array['name'];
    $this->price = $array['price'];
    $this->type = $array['type'];
    $this->weight = $array['b_weight'];
  }

  //render object
  public function displayProduct(){
    echo "<div class='col-md-2 product'>
      <input type='checkbox' class='check-box' name='delete' value='" . $this->id . "'/>
      <p>" . $this->sku . "</p>
      <p>" . $this->name . "</p>
      <p>" . $this->price . " $</p>
      <p>Weight: " . $this->weight . "Kg</p>
      </div>";
  }

  public function formHtml()
  {
    echo " <div class='form-group row'>
        <label for='inputWeight' class='col-sm-1 col-form-label'>Book weight</label>
        <div class='col-sm-5'>
            <input type='number' step=0.01 name='data[b_weight]' class='form-control' id='inputWeight' placeholder='Book weight in Kg...'>
          <div class='col-sm-6'></div>
        </div>
      </div> ";
  }
}
